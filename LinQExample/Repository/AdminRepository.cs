﻿using LinQExample.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQExample.Repository
{
    public class AdminRepository : IAdminRepository
    {
        public List<Admin> GetAdmins()
        {

            var jsonData = @"[{
                ""id"": 1,
  ""email"": ""ztomicki0@hp.com"",
  ""password"": ""Ov9c0rJi38s""
}, {
                ""id"": 2,
  ""email"": ""ddrews1@hexun.com"",
  ""password"": ""We3qs6P""
         }, {
                ""id"": 3,
  ""email"": ""csimoes2@themeforest.net"",
  ""password"": ""yYnuSg""
         }, {
                ""id"": 4,
  ""email"": ""ltimson3@ezinearticles.com"",
  ""password"": ""4krvqv1HOaH""
         }, {
                ""id"": 5,
  ""email"": ""galldis4@taobao.com"",
  ""password"": ""6As2dN""
         }]";
            var result = JsonConvert.DeserializeObject<List<Admin>>(jsonData);
            return result;
        }
    }
}
