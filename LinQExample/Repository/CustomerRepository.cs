﻿using LinQExample.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQExample.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetCustomer()
        {
            var json = @"[{
                ""CustomerId"": 1,
  ""FirstName"": ""Shanan"",
  ""LastName"": ""Lightewood"",
  ""Email"": ""slightewood0@jigsy.com"",
  ""Mobile"": ""864 883 9004""
}, {
                ""CustomerId"": 2,
  ""FirstName"": ""Manon"",
  ""LastName"": ""Dumphrey"",
  ""Email"": ""mdumphrey1@devhub.com"",
  ""Mobile"": ""512 450 5623""
         }, {
                ""CustomerId"": 3,
  ""FirstName"": ""Allyn"",
  ""LastName"": ""Paxton"",
  ""Email"": ""apaxton2@soup.io"",
  ""Mobile"": ""892 373 5103""
         }, {
                ""CustomerId"": 4,
  ""FirstName"": ""Ginevra"",
  ""LastName"": ""Collister"",
  ""Email"": ""gcollister3@yellowpages.com"",
  ""Mobile"": ""763 410 8486""
         }, {
                ""CustomerId"": 5,
  ""FirstName"": ""Elsi"",
  ""LastName"": ""Wotherspoon"",
  ""Email"": ""ewotherspoon4@globo.com"",
  ""Mobile"": ""395 563 4926""
         }, {
                ""CustomerId"": 6,
  ""FirstName"": ""Penny"",
  ""LastName"": ""Keen"",
  ""Email"": ""pkeen5@mashable.com"",
  ""Mobile"": ""741 662 3843""
         }, {
                ""CustomerId"": 7,
  ""FirstName"": ""Graham"",
  ""LastName"": ""Commucci"",
  ""Email"": ""gcommucci6@amazon.co.uk"",
  ""Mobile"": ""212 852 1169""
         }, {
                ""CustomerId"": 8,
  ""FirstName"": ""Ellsworth"",
  ""LastName"": ""Kimberly"",
  ""Email"": ""ekimberly7@xing.com"",
  ""Mobile"": ""131 675 2199""
         }, {
                ""CustomerId"": 9,
  ""FirstName"": ""Elly"",
  ""LastName"": ""Fadell"",
  ""Email"": ""efadell8@cisco.com"",
  ""Mobile"": ""982 560 2648""
         }, {
                ""CustomerId"": 10,
  ""FirstName"": ""Barth"",
  ""LastName"": ""Goodman"",
  ""Email"": ""bgoodman9@free.fr"",
  ""Mobile"": ""731 606 0386""
         }, {
                ""CustomerId"": 11,
  ""FirstName"": ""Robin"",
  ""LastName"": ""Hazard"",
  ""Email"": ""rhazarda@free.fr"",
  ""Mobile"": ""132 609 5555""
         }, {
                ""CustomerId"": 12,
  ""FirstName"": ""Mayor"",
  ""LastName"": ""Dauby"",
  ""Email"": ""mdaubyb@ucoz.com"",
  ""Mobile"": ""666 951 7896""
         }, {
                ""CustomerId"": 13,
  ""FirstName"": ""Jsandye"",
  ""LastName"": ""Radoux"",
  ""Email"": ""jradouxc@ca.gov"",
  ""Mobile"": ""526 691 5566""
         }, {
                ""CustomerId"": 14,
  ""FirstName"": ""Blondie"",
  ""LastName"": ""Rosthorn"",
  ""Email"": ""brosthornd@boston.com"",
  ""Mobile"": ""179 514 8310""
         }, {
                ""CustomerId"": 15,
  ""FirstName"": ""Wrennie"",
  ""LastName"": ""Dufty"",
  ""Email"": ""wduftye@ebay.co.uk"",
  ""Mobile"": ""492 937 6761""
         }, {
                ""CustomerId"": 16,
  ""FirstName"": ""Matilde"",
  ""LastName"": ""Aspell"",
  ""Email"": ""maspellf@boston.com"",
  ""Mobile"": ""596 456 9432""
         }, {
                ""CustomerId"": 17,
  ""FirstName"": ""Tore"",
  ""LastName"": ""Northall"",
  ""Email"": ""tnorthallg@java.com"",
  ""Mobile"": ""366 526 0305""
         }, {
                ""CustomerId"": 18,
  ""FirstName"": ""Lori"",
  ""LastName"": ""Brumble"",
  ""Email"": ""lbrumbleh@princeton.edu"",
  ""Mobile"": ""796 551 4248""
         }, {
                ""CustomerId"": 19,
  ""FirstName"": ""Gerard"",
  ""LastName"": ""Guerreau"",
  ""Email"": ""gguerreaui@vistaprint.com"",
  ""Mobile"": ""574 385 7042""
         }, {
                ""CustomerId"": 20,
  ""FirstName"": ""Carree"",
  ""LastName"": ""Becraft"",
  ""Email"": ""cbecraftj@cbslocal.com"",
  ""Mobile"": ""564 704 5603""
         }, {
                ""CustomerId"": 21,
  ""FirstName"": ""Marja"",
  ""LastName"": ""Vanshin"",
  ""Email"": ""mvanshink@jigsy.com"",
  ""Mobile"": ""490 646 5642""
         }, {
                ""CustomerId"": 22,
  ""FirstName"": ""Barthel"",
  ""LastName"": ""Stuehmeier"",
  ""Email"": ""bstuehmeierl@blogtalkradio.com"",
  ""Mobile"": ""835 526 2897""
         }, {
                ""CustomerId"": 23,
  ""FirstName"": ""Keefer"",
  ""LastName"": ""Rosengart"",
  ""Email"": ""krosengartm@sohu.com"",
  ""Mobile"": ""965 543 0719""
         }, {
                ""CustomerId"": 24,
  ""FirstName"": ""Windham"",
  ""LastName"": ""Renac"",
  ""Email"": ""wrenacn@huffingtonpost.com"",
  ""Mobile"": ""871 313 6951""
         }, {
                ""CustomerId"": 25,
  ""FirstName"": ""Audrye"",
  ""LastName"": ""Martinet"",
  ""Email"": ""amartineto@baidu.com"",
  ""Mobile"": ""540 676 7877""
         }, {
                ""CustomerId"": 26,
  ""FirstName"": ""Yvette"",
  ""LastName"": ""Emor"",
  ""Email"": ""yemorp@ow.ly"",
  ""Mobile"": ""743 445 1330""
         }, {
                ""CustomerId"": 27,
  ""FirstName"": ""Natal"",
  ""LastName"": ""Montilla"",
  ""Email"": ""nmontillaq@123-reg.co.uk"",
  ""Mobile"": ""707 640 5614""
         }, {
                ""CustomerId"": 28,
  ""FirstName"": ""Trefor"",
  ""LastName"": ""Franzotto"",
  ""Email"": ""tfranzottor@hatena.ne.jp"",
  ""Mobile"": ""507 658 4011""
         }, {
                ""CustomerId"": 29,
  ""FirstName"": ""Brant"",
  ""LastName"": ""Rochford"",
  ""Email"": ""brochfords@so-net.ne.jp"",
  ""Mobile"": ""382 786 4152""
         }, {
                ""CustomerId"": 30,
  ""FirstName"": ""Dall"",
  ""LastName"": ""Janowicz"",
  ""Email"": ""djanowiczt@flickr.com"",
  ""Mobile"": ""500 845 6208""
         }, {
                ""CustomerId"": 31,
  ""FirstName"": ""Hewe"",
  ""LastName"": ""Brownsea"",
  ""Email"": ""hbrownseau@vk.com"",
  ""Mobile"": ""211 571 3146""
         }, {
                ""CustomerId"": 32,
  ""FirstName"": ""Wood"",
  ""LastName"": ""McMenamie"",
  ""Email"": ""wmcmenamiev@networksolutions.com"",
  ""Mobile"": ""238 175 3583""
         }, {
                ""CustomerId"": 33,
  ""FirstName"": ""Scottie"",
  ""LastName"": ""Courtonne"",
  ""Email"": ""scourtonnew@wiley.com"",
  ""Mobile"": ""536 208 8296""
         }, {
                ""CustomerId"": 34,
  ""FirstName"": ""Pamella"",
  ""LastName"": ""Beig"",
  ""Email"": ""pbeigx@yelp.com"",
  ""Mobile"": ""493 167 5854""
         }, {
                ""CustomerId"": 35,
  ""FirstName"": ""Caro"",
  ""LastName"": ""Spare"",
  ""Email"": ""csparey@umn.edu"",
  ""Mobile"": ""975 720 6733""
         }, {
                ""CustomerId"": 36,
  ""FirstName"": ""Kelvin"",
  ""LastName"": ""Wiz"",
  ""Email"": ""kwizz@privacy.gov.au"",
  ""Mobile"": ""227 924 5970""
         }, {
                ""CustomerId"": 37,
  ""FirstName"": ""Joey"",
  ""LastName"": ""Deacock"",
  ""Email"": ""jdeacock10@pbs.org"",
  ""Mobile"": ""300 370 0018""
         }, {
                ""CustomerId"": 38,
  ""FirstName"": ""Francois"",
  ""LastName"": ""Dilliston"",
  ""Email"": ""fdilliston11@google.co.uk"",
  ""Mobile"": ""343 134 4316""
         }, {
                ""CustomerId"": 39,
  ""FirstName"": ""Hercule"",
  ""LastName"": ""Westbrook"",
  ""Email"": ""hwestbrook12@ted.com"",
  ""Mobile"": ""851 803 5911""
         }, {
                ""CustomerId"": 40,
  ""FirstName"": ""Elna"",
  ""LastName"": ""Colam"",
  ""Email"": ""ecolam13@istockphoto.com"",
  ""Mobile"": ""509 866 9988""
         }, {
                ""CustomerId"": 41,
  ""FirstName"": ""Katrine"",
  ""LastName"": ""Bostick"",
  ""Email"": ""kbostick14@latimes.com"",
  ""Mobile"": ""254 997 1123""
         }, {
                ""CustomerId"": 42,
  ""FirstName"": ""Timmy"",
  ""LastName"": ""Toal"",
  ""Email"": ""ttoal15@mashable.com"",
  ""Mobile"": ""936 577 3685""
         }, {
                ""CustomerId"": 43,
  ""FirstName"": ""Tammy"",
  ""LastName"": ""Redihalgh"",
  ""Email"": ""tredihalgh16@answers.com"",
  ""Mobile"": ""188 627 9048""
         }, {
                ""CustomerId"": 44,
  ""FirstName"": ""Rafaelita"",
  ""LastName"": ""Couser"",
  ""Email"": ""rcouser17@creativecommons.org"",
  ""Mobile"": ""248 806 3810""
         }, {
                ""CustomerId"": 45,
  ""FirstName"": ""Avie"",
  ""LastName"": ""Railton"",
  ""Email"": ""arailton18@webmd.com"",
  ""Mobile"": ""928 579 3762""
         }, {
                ""CustomerId"": 46,
  ""FirstName"": ""Cindie"",
  ""LastName"": ""Puckinghorne"",
  ""Email"": ""cpuckinghorne19@boston.com"",
  ""Mobile"": ""942 256 8964""
         }, {
                ""CustomerId"": 47,
  ""FirstName"": ""Leona"",
  ""LastName"": ""Templeman"",
  ""Email"": ""ltempleman1a@booking.com"",
  ""Mobile"": ""984 996 4265""
         }, {
                ""CustomerId"": 48,
  ""FirstName"": ""Ronnie"",
  ""LastName"": ""Bruggen"",
  ""Email"": ""rbruggen1b@nature.com"",
  ""Mobile"": ""845 555 1299""
         }, {
                ""CustomerId"": 49,
  ""FirstName"": ""Selie"",
  ""LastName"": ""Corbishley"",
  ""Email"": ""scorbishley1c@thetimes.co.uk"",
  ""Mobile"": ""254 404 2145""
         }, {
                ""CustomerId"": 50,
  ""FirstName"": ""Penelopa"",
  ""LastName"": ""Rediers"",
  ""Email"": ""prediers1d@wiley.com"",
  ""Mobile"": ""757 929 4044""
         }, {
                ""CustomerId"": 51,
  ""FirstName"": ""Inez"",
  ""LastName"": ""Cartin"",
  ""Email"": ""icartin1e@stanford.edu"",
  ""Mobile"": ""767 664 4529""
         }, {
                ""CustomerId"": 52,
  ""FirstName"": ""Craggy"",
  ""LastName"": ""Goold"",
  ""Email"": ""cgoold1f@biblegateway.com"",
  ""Mobile"": ""622 934 9858""
         }, {
                ""CustomerId"": 53,
  ""FirstName"": ""Ferdinande"",
  ""LastName"": ""Seathwright"",
  ""Email"": ""fseathwright1g@prweb.com"",
  ""Mobile"": ""592 446 4121""
         }, {
                ""CustomerId"": 54,
  ""FirstName"": ""Aliza"",
  ""LastName"": ""Moyce"",
  ""Email"": ""amoyce1h@columbia.edu"",
  ""Mobile"": ""559 963 4693""
         }, {
                ""CustomerId"": 55,
  ""FirstName"": ""Valentin"",
  ""LastName"": ""Chasemore"",
  ""Email"": ""vchasemore1i@alexa.com"",
  ""Mobile"": ""328 771 3751""
         }, {
                ""CustomerId"": 56,
  ""FirstName"": ""Jamal"",
  ""LastName"": ""Sundin"",
  ""Email"": ""jsundin1j@vk.com"",
  ""Mobile"": ""520 405 0574""
         }, {
                ""CustomerId"": 57,
  ""FirstName"": ""Keslie"",
  ""LastName"": ""Pinney"",
  ""Email"": ""kpinney1k@live.com"",
  ""Mobile"": ""310 485 4188""
         }, {
                ""CustomerId"": 58,
  ""FirstName"": ""Agata"",
  ""LastName"": ""Climson"",
  ""Email"": ""aclimson1l@is.gd"",
  ""Mobile"": ""559 591 4054""
         }, {
                ""CustomerId"": 59,
  ""FirstName"": ""Aeriela"",
  ""LastName"": ""Noblett"",
  ""Email"": ""anoblett1m@toplist.cz"",
  ""Mobile"": ""901 168 2728""
         }, {
                ""CustomerId"": 60,
  ""FirstName"": ""Dorette"",
  ""LastName"": ""Stryde"",
  ""Email"": ""dstryde1n@issuu.com"",
  ""Mobile"": ""561 796 0468""
         }, {
                ""CustomerId"": 61,
  ""FirstName"": ""Whitney"",
  ""LastName"": ""Bolzmann"",
  ""Email"": ""wbolzmann1o@examiner.com"",
  ""Mobile"": ""530 720 8675""
         }, {
                ""CustomerId"": 62,
  ""FirstName"": ""Frans"",
  ""LastName"": ""Winkett"",
  ""Email"": ""fwinkett1p@chron.com"",
  ""Mobile"": ""198 895 1967""
         }, {
                ""CustomerId"": 63,
  ""FirstName"": ""Elliot"",
  ""LastName"": ""Tinwell"",
  ""Email"": ""etinwell1q@wp.com"",
  ""Mobile"": ""342 656 3064""
         }, {
                ""CustomerId"": 64,
  ""FirstName"": ""Salvatore"",
  ""LastName"": ""Aberdeen"",
  ""Email"": ""saberdeen1r@techcrunch.com"",
  ""Mobile"": ""883 490 9096""
         }, {
                ""CustomerId"": 65,
  ""FirstName"": ""Dalenna"",
  ""LastName"": ""Vina"",
  ""Email"": ""dvina1s@disqus.com"",
  ""Mobile"": ""401 264 8384""
         }, {
                ""CustomerId"": 66,
  ""FirstName"": ""Cordie"",
  ""LastName"": ""Simmonett"",
  ""Email"": ""csimmonett1t@sina.com.cn"",
  ""Mobile"": ""716 733 5267""
         }, {
                ""CustomerId"": 67,
  ""FirstName"": ""Olivier"",
  ""LastName"": ""Koeppke"",
  ""Email"": ""okoeppke1u@sakura.ne.jp"",
  ""Mobile"": ""334 906 2220""
         }, {
                ""CustomerId"": 68,
  ""FirstName"": ""Nissie"",
  ""LastName"": ""Fitzmaurice"",
  ""Email"": ""nfitzmaurice1v@globo.com"",
  ""Mobile"": ""266 650 3090""
         }, {
                ""CustomerId"": 69,
  ""FirstName"": ""Sibeal"",
  ""LastName"": ""De Mitri"",
  ""Email"": ""sdemitri1w@paypal.com"",
  ""Mobile"": ""636 912 1310""
         }, {
                ""CustomerId"": 70,
  ""FirstName"": ""Lisetta"",
  ""LastName"": ""Matyasik"",
  ""Email"": ""lmatyasik1x@umich.edu"",
  ""Mobile"": ""762 429 9204""
         }, {
                ""CustomerId"": 71,
  ""FirstName"": ""Shelba"",
  ""LastName"": ""Daventry"",
  ""Email"": ""sdaventry1y@boston.com"",
  ""Mobile"": ""571 510 5187""
         }, {
                ""CustomerId"": 72,
  ""FirstName"": ""Axe"",
  ""LastName"": ""Gabala"",
  ""Email"": ""agabala1z@freewebs.com"",
  ""Mobile"": ""433 883 5105""
         }, {
                ""CustomerId"": 73,
  ""FirstName"": ""Baillie"",
  ""LastName"": ""Stearne"",
  ""Email"": ""bstearne20@sakura.ne.jp"",
  ""Mobile"": ""783 313 5716""
         }, {
                ""CustomerId"": 74,
  ""FirstName"": ""Carly"",
  ""LastName"": ""Esome"",
  ""Email"": ""cesome21@joomla.org"",
  ""Mobile"": ""807 662 7213""
         }, {
                ""CustomerId"": 75,
  ""FirstName"": ""Hastings"",
  ""LastName"": ""Lamping"",
  ""Email"": ""hlamping22@discuz.net"",
  ""Mobile"": ""918 166 0473""
         }, {
                ""CustomerId"": 76,
  ""FirstName"": ""Loretta"",
  ""LastName"": ""Gaydon"",
  ""Email"": ""lgaydon23@delicious.com"",
  ""Mobile"": ""793 274 1559""
         }, {
                ""CustomerId"": 77,
  ""FirstName"": ""Rhea"",
  ""LastName"": ""Radnedge"",
  ""Email"": ""rradnedge24@addtoany.com"",
  ""Mobile"": ""320 303 2766""
         }, {
                ""CustomerId"": 78,
  ""FirstName"": ""Jessa"",
  ""LastName"": ""Berrisford"",
  ""Email"": ""jberrisford25@aol.com"",
  ""Mobile"": ""212 897 3198""
         }, {
                ""CustomerId"": 79,
  ""FirstName"": ""Neils"",
  ""LastName"": ""Gamlyn"",
  ""Email"": ""ngamlyn26@mapquest.com"",
  ""Mobile"": ""893 111 7111""
         }, {
                ""CustomerId"": 80,
  ""FirstName"": ""Tiertza"",
  ""LastName"": ""Alderton"",
  ""Email"": ""talderton27@thetimes.co.uk"",
  ""Mobile"": ""148 194 2110""
         }, {
                ""CustomerId"": 81,
  ""FirstName"": ""Hill"",
  ""LastName"": ""Glason"",
  ""Email"": ""hglason28@netvibes.com"",
  ""Mobile"": ""631 923 0705""
         }, {
                ""CustomerId"": 82,
  ""FirstName"": ""Peter"",
  ""LastName"": ""Collard"",
  ""Email"": ""pcollard29@desdev.cn"",
  ""Mobile"": ""260 636 5977""
         }, {
                ""CustomerId"": 83,
  ""FirstName"": ""Wain"",
  ""LastName"": ""Elwyn"",
  ""Email"": ""welwyn2a@unblog.fr"",
  ""Mobile"": ""822 709 5836""
         }, {
                ""CustomerId"": 84,
  ""FirstName"": ""Lauren"",
  ""LastName"": ""Sallter"",
  ""Email"": ""lsallter2b@apache.org"",
  ""Mobile"": ""319 324 3004""
         }, {
                ""CustomerId"": 85,
  ""FirstName"": ""Adler"",
  ""LastName"": ""Scardifield"",
  ""Email"": ""ascardifield2c@bloomberg.com"",
  ""Mobile"": ""807 847 7199""
         }, {
                ""CustomerId"": 86,
  ""FirstName"": ""Nicol"",
  ""LastName"": ""Simonard"",
  ""Email"": ""nsimonard2d@irs.gov"",
  ""Mobile"": ""909 451 6363""
         }, {
                ""CustomerId"": 87,
  ""FirstName"": ""Shane"",
  ""LastName"": ""Arunowicz"",
  ""Email"": ""sarunowicz2e@seattletimes.com"",
  ""Mobile"": ""959 323 7005""
         }, {
                ""CustomerId"": 88,
  ""FirstName"": ""Berrie"",
  ""LastName"": ""Taylerson"",
  ""Email"": ""btaylerson2f@harvard.edu"",
  ""Mobile"": ""617 952 4131""
         }, {
                ""CustomerId"": 89,
  ""FirstName"": ""Gerik"",
  ""LastName"": ""Selby"",
  ""Email"": ""gselby2g@uol.com.br"",
  ""Mobile"": ""494 926 6217""
         }, {
                ""CustomerId"": 90,
  ""FirstName"": ""Issiah"",
  ""LastName"": ""Yitzhakof"",
  ""Email"": ""iyitzhakof2h@china.com.cn"",
  ""Mobile"": ""968 930 4056""
         }, {
                ""CustomerId"": 91,
  ""FirstName"": ""Andrej"",
  ""LastName"": ""Penvarne"",
  ""Email"": ""apenvarne2i@creativecommons.org"",
  ""Mobile"": ""878 680 6043""
         }, {
                ""CustomerId"": 92,
  ""FirstName"": ""Randie"",
  ""LastName"": ""Potapczuk"",
  ""Email"": ""rpotapczuk2j@tiny.cc"",
  ""Mobile"": ""477 667 3383""
         }, {
                ""CustomerId"": 93,
  ""FirstName"": ""Sherwynd"",
  ""LastName"": ""Flemyng"",
  ""Email"": ""sflemyng2k@issuu.com"",
  ""Mobile"": ""928 383 7129""
         }, {
                ""CustomerId"": 94,
  ""FirstName"": ""Lavena"",
  ""LastName"": ""Dyte"",
  ""Email"": ""ldyte2l@ibm.com"",
  ""Mobile"": ""694 800 5909""
         }, {
                ""CustomerId"": 95,
  ""FirstName"": ""Christie"",
  ""LastName"": ""Flippen"",
  ""Email"": ""cflippen2m@vkontakte.ru"",
  ""Mobile"": ""154 568 5279""
         }, {
                ""CustomerId"": 96,
  ""FirstName"": ""Arthur"",
  ""LastName"": ""Workes"",
  ""Email"": ""aworkes2n@github.com"",
  ""Mobile"": ""614 918 5051""
         }, {
                ""CustomerId"": 97,
  ""FirstName"": ""Drugi"",
  ""LastName"": ""Brimson"",
  ""Email"": ""dbrimson2o@harvard.edu"",
  ""Mobile"": ""686 166 3351""
         }, {
                ""CustomerId"": 98,
  ""FirstName"": ""Denver"",
  ""LastName"": ""Cadd"",
  ""Email"": ""dcadd2p@quantcast.com"",
  ""Mobile"": ""740 100 6236""
         }, {
                ""CustomerId"": 99,
  ""FirstName"": ""Marylee"",
  ""LastName"": ""Friett"",
  ""Email"": ""mfriett2q@sina.com.cn"",
  ""Mobile"": ""316 813 4976""
         }, {
                ""CustomerId"": 100,
  ""FirstName"": ""Wylma"",
  ""LastName"": ""McGing"",
  ""Email"": ""wmcging2r@gravatar.com"",
  ""Mobile"": ""889 741 5101""
         }]";
            var result = JsonConvert.DeserializeObject<List<Customer>>(json);
            return result;
        }
    }
}
