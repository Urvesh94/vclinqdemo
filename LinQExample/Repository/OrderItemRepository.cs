﻿using LinQExample.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQExample.Repository
{
    public class OrderItemRepository : IOrderItemRepository
    {
        public List<OrderItem> GetOrderItems()
        {

            var JSON = @"[{
  ""OrderItemId"": 1,
  ""OrderId"": 1,
  ""ProductId"": 1,
  ""Price"": ""error: wrong number of arguments (given 4, expected 3)"",
  ""Mobile"": ""196-257-5647""
}, {
  ""OrderItemId"": 2,
  ""OrderId"": 2,
  ""ProductId"": 2,
  ""Price"": ""error: wrong number of arguments (given 4, expected 3)"",
  ""Mobile"": ""713-519-7578""
}, {
  ""OrderItemId"": 3,
  ""OrderId"": 3,
  ""ProductId"": 3,
  ""Price"": ""error: wrong number of arguments (given 4, expected 3)"",
  ""Mobile"": ""816-587-6502""
}, {
  ""OrderItemId"": 4,
  ""OrderId"": 4,
  ""ProductId"": 4,
  ""Price"": ""error: wrong number of arguments (given 4, expected 3)"",
  ""Mobile"": ""914-156-7072""
}, {
  ""OrderItemId"": 5,
  ""OrderId"": 5,
  ""ProductId"": 5,
  ""Price"": ""error: wrong number of arguments (given 4, expected 3)"",
  ""Mobile"": ""694-497-8676""
}, {
  ""OrderItemId"": 6,
  ""OrderId"": 6,
  ""ProductId"": 6,
  ""Price"": ""error: wrong number of arguments (given 4, expected 3)"",
  ""Mobile"": ""425-340-0514""
}, {
  ""OrderItemId"": 7,
  ""OrderId"": 7,
  ""ProductId"": 7,
  ""Price"": ""error: wrong number of arguments (given 4, expected 3)"",
  ""Mobile"": ""960-974-5238""
}]";
            var result = JsonConvert.DeserializeObject<List<OrderItem>>(JSON);

            return result;
        }
    }
}
