﻿using LinQExample.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQExample.Repository
{
    public class OrderRepository : IorderRepository
    {
        public List<Order> GetOrders()
        {
            var json = @"[{
                ""OrderId"": 1,
  ""CustomerId"": 1,
  ""TotalPrice"": 1,
  ""Status"": null
}, {
                ""OrderId"": 2,
  ""CustomerId"": 2,
  ""TotalPrice"": 2,
  ""Status"": null
         }, {
                ""OrderId"": 3,
  ""CustomerId"": 3,
  ""TotalPrice"": 3,
  ""Status"": null
         }, {
                ""OrderId"": 4,
  ""CustomerId"": 4,
  ""TotalPrice"": 4,
  ""Status"": null
         }, {
                ""OrderId"": 5,
  ""CustomerId"": 5,
  ""TotalPrice"": 5,
  ""Status"": null
         }, {
                ""OrderId"": 6,
  ""CustomerId"": 6,
  ""TotalPrice"": 6,
  ""Status"": null
         }, {
                ""OrderId"": 7,
  ""CustomerId"": 7,
  ""TotalPrice"": 7,
  ""Status"": null
         }, {
                ""OrderId"": 8,
  ""CustomerId"": 8,
  ""TotalPrice"": 8,
  ""Status"": null
         }, {
                ""OrderId"": 9,
  ""CustomerId"": 9,
  ""TotalPrice"": 9,
  ""Status"": null
         }, {
                ""OrderId"": 10,
  ""CustomerId"": 10,
  ""TotalPrice"": 10,
  ""Status"": null
         }, {
                ""OrderId"": 11,
  ""CustomerId"": 11,
  ""TotalPrice"": 11,
  ""Status"": null
         }, {
                ""OrderId"": 12,
  ""CustomerId"": 12,
  ""TotalPrice"": 12,
  ""Status"": null
         }, {
                ""OrderId"": 13,
  ""CustomerId"": 13,
  ""TotalPrice"": 13,
  ""Status"": null
         }, {
                ""OrderId"": 14,
  ""CustomerId"": 14,
  ""TotalPrice"": 14,
  ""Status"": null
         }, {
                ""OrderId"": 15,
  ""CustomerId"": 15,
  ""TotalPrice"": 15,
  ""Status"": null
         }]";
            var result = JsonConvert.DeserializeObject<List<Order>>(json);
            return result;
        }
    }
}
