﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinQExample.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public int CustomerId { get; set; }
    
        public decimal TotalPrice { get; set; }
        public Enum Status { get; set; }
    }
}
